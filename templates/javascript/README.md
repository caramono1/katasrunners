# Javascript kata template

This template uses [Jasmine](https://jasmine.github.io/) to run the tests. In order to execute the tests, you will need to install NodeJS and NPM.

## Running the tests

`npm test`

## Installation

- First install NodeJS. You can find the details [on their website](https://nodejs.org/en/). This will install NodeJS and NPM.
- Install the dependencies using NPM: `npm install`
