# Python kata template

This template uses the default Python [unittest](https://docs.python.org/3.5/library/unittest.html) library to run the tests. So no extra dependencies out of Python3.

## Running the tests

`python3 -m unittest test_example.py`

## Installation

- Install Python 3. You can find the details [on their website](https://www.python.org/downloads/).
