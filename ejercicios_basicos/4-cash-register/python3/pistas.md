# Python

# Cash Register

## Problem explanation

Tienes que crear un programa que devolverá un objeto que contenga una clave de estado y una clave con el cambio. El valor del estado es el texto `INSUFFICIENT_FUNDS`, `CLOSED` o `OPEN`, y el valor del cambio es un array de dos dimensiones con el cambio a devolver.

## Pistas

### Pista 1

Es más fácil cuando sabes cuanto dinero tienes en tu caja registradora al principio. Para ello, es recomendable una función que calcule esto. Entonces, podrás ver si tienes suficiente dinero para completar la transacción y devolver el cambio o si deberías cerrar la caja.

### Pista 2

El problema es más fácil cuando sabes el valor de cada billete o moneda con el que estas trabajando, en vez de saber la suma total de ellos en la caja. Por ejemplo, es útil saber que un `nickel` vale `0.05`, junto con el hecho de que en total tienes `$2.05` en `nickels` en la caja.

### Pista 3

Tendrás que obtener el máximo cambio posible de cada tipo de billete o moneda antes de moverte al siguiente, desde el de mayor a menor valor. Sigue hasta que allas calculado todo el cambio a devolver.
