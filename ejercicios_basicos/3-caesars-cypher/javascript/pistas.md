# Pistas

## Pista 1

Usa `String.charCodeAt()` para convertir los caracteres en inglés a ASCII.

## Pista 2

Usa `String.fromCharCode()` para convertir el ASCII a caracteres en inglés.

## Pista 3

Deja cualquier cosa que no este entre A-Z sin tocar.
