import unittest
from palindrome import palindrome

class TestPalindrome(unittest.TestCase):
    def test_a_palindrome_should_return_boolean(self):
        self.assertIsInstance(palindrome("eye"), bool)

    def test_b_should_return_true_when_given_a_palindrome(self):
        self.assertTrue(palindrome("eye"))

    def test_c_should_return_false_when_not_given_a_palindrome(self):
        self.assertFalse(palindrome("not palindrome"))

    def test_d_should_ignore_word_casing(self):
        self.assertFalse(palindrome("Eye"))

    def test_e_should_ignore_non_alphanumeric_characters(self):
        self.assertFalse(palindrome("My age is 0, 0 si ega ym."))
