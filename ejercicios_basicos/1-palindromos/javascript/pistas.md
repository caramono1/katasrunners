Nuestro objetivo para resolver este problema es limpiar la cadena de texto pasada y comprobar si es un palíndromo.

- Si no tienes claro que es un palíndromo, es una palabra o frase que cuando se le da la vuelva, se lee igual al revés que en el orden inicial. Un ejemplo sencillo es `efe`, cuando le das la vuelta a las letras, ¡dice lo mismo! Otro ejemplo de palíndromo es `Yo soy`, que se lee igual ¡del derecho o del revés!

Una vez hemos determinado si es un palíndromo o no, queremos devolver si es `true` o `false` basado en lo que encontremos.

## Enlaces relevantes
- [str.replace](https://docs.python.org/3.3/library/stdtypes.html#str.replace)
- [str.lower](https://docs.python.org/3.3/library/stdtypes.html#str.lower)

## Pistas

### Pista 1
Las expresiones regulares, `RegEx`, pueden ser usadas para eliminar caracteres que no queremos de un string. Si necesitas ayuda con las expresiones regulares, prueba con [esta web](https://regexr.com/)

### Pista 2

Los métodos [`String.prototype.split`](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/String/split) y [`Array.prototype.join`](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/join) pueden ser usados aquí. Los bucles de `for` y `while` son otra alternativa o puedes incluso usar un `map`!

### Pista 3

Puedes usar [`String.prototype.toLowerCase`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/toLowerCase) para pasar una cadena de texto a minúsculas.

